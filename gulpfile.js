// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const exec = require('child_process').exec;


// File paths
const files = {
  scssPath: 'scss/index.scss',
  watchPath: 'scss/**/*.scss'
}

// Sass task: compiles the style.scss file into style.css
function scssTask() {
  return src(files.scssPath)
    .pipe(sourcemaps.init()) // initialize sourcemaps first
    .pipe(sass().on('error', sass.logError)) // compile SCSS to CSS
    .pipe(postcss([autoprefixer(), cssnano()])) // PostCSS plugins
    .pipe(concat('all.css'))
    .pipe(sourcemaps.write('./')) // write sourcemaps file in current directory
    .pipe(dest('./css/')); // put final CSS in home folder
}

// delete old build folder

function cleanTask() {
  return src('./build/', { allowEmpty: true  })
    .pipe(clean())
}
// create build folder
// File paths
const buildFiles = [
    './css/all.css',
    './style.css',
    './img/**/*.*',
    './js/**/*.*',
    './webfonts/**/*.*'
  ]
function buildTask() {
  return src(buildFiles, { base: './' })
    .pipe(dest('./build/'));
}


// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask() {
  watch([files.watchPath],
    { interval: 1000, usePolling: true }, //Makes docker work
    series(
      parallel(scssTask)
    )
  );
}



exports.default = series(
  parallel(scssTask),
  watchTask);


exports.build = series(
  parallel(cleanTask),
  buildTask);
